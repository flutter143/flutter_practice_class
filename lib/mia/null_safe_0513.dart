void main(List<String> arguments) {
  //driveTwoSeatedCar('WCKD');
  print(mustReturnString());
  print(multipleReturns());
}

void doesNotReturnAnyting() {}

String mustReturnString() {
  return 'Must return a String in null safety';
}

String multipleReturns() {
  if ('test' == 'test') {
    return 'true';
  } else if ('test' == 'notTest') {
    return 'false';
  } else if ('test' != 'whatever') {
    return 'whatever';
  } else {
    // return 'wlee,it\'s done';
    throw ArgumentError(); //! return Never
  }
}
