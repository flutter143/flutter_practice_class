//!top-level variable
import 'dart:ui';

int t = 5;
int? nullableTopLevel;
late int nonNullableTopLevel;

class A {
  //!static variable
  static int s = 12;
  static int? nullableStatic = 5;
  static late int nonNullableStatic;
  int? a;

  //!instance variable (field/property
  double i = 25;

  // double nullableInstance=1;
  double? nullableInstance;

  A({
    required this.i,
    // required this.nullableInstance,
  }) {
    nullableInstance = 30;
    // ? this is the constructor body
  }
}

void randomFunction() {
  //!local variable
  int l = 4;
}

void main(List<String> arguments) {
  t = 8;
  nullableTopLevel = null;
  //print(nullableTopLevel!.isEven);

  //nonNullableTopLevel=10;
  print(nonNullableTopLevel.isEven);

  // A -> class
  // s -> 為A 裡面定義的static 變數
  print(A.s);
  // A.nullableStatic = null;
  // print(A.nullableStatic!.isEven);

  A.nullableStatic = 5;
  //print(A.nullableStatic!.isEven
  A.nonNullableStatic = 15;
  //print(A.nonNullableStatic.isEven

  // print(A(i:20).i);
  //A(i: 10)=>利用constructor（建構函式）建立物件
  //A(i: 10)..nullableInstance=>調用物件的instance nullableInstance的變數
  var alfa = A(i: 10)..nullableInstance = 20;
  print(alfa.nullableInstance);

  var vi = 5; //int vi=5
  var vs = 'Example'; //String vs='Example'
  var vl = [1, 2, 3];
  print('var runtimeTypes:\n');
  print(vi.runtimeType);
  print(vs.runtimeType);
  print(vl.runtimeType);

  //動態型別，因為可變型別
  dynamic di = 5;
  dynamic ds = 'Example';
  dynamic dl = [1, 2, 3];
  print('\ndynamic runtimeTypes:\n');
  print(di.runtimeType);
  print(ds.runtimeType);
  print(dl.runtimeType);

  const int aa = 5; //int a=const int(value:5)
  //! a is const,so it const be reassigned
  // a = 7;
  // a = 8;
  // a = 9;

  //! list is const, then it means [1,2,3] is const
  const list = [1, 2, 3];

  //ignore: lines_longer_than_80_chars
  //! if the value is const [1,2,3], it DOES NOT MEAN variable list2 is also const!
  var list2 = const [1, 2, 3];
  final b = 6;
  //! b can't be reassigned since it's final
  // b = 7;

  //! final lost3 DOES NOT MEAN [1,2,3] list is const!
  final list3 = [1, 2, 3];

  //! since [1,2,3] is not const, you can mutate it's const
  list3.add(4);
  list3.add(5);
  list3.add(6);

  //! yet list3 is still a final variable, so it can't be reassigned
  // list3 = [4,5,6];

  //! now [1,2,3] is a const list
  //! final vars will have value known at runtime
  final list4 = const [1, 2, 3]; //const list4=[1,2,3];

  list4.add(5);
  list4.add(6);
  list4.add(7);

  //! const vars will have a value known at compile time
  const list5 = [1, 2, 3];

  print(list4.hashCode);
  print(list5.hashCode);

  final alfa1 = A(i: 1);
  alfa.a = 10;
  print(alfa.a);

  final sum = calculateSum();
  var c1 = const C(list: [1, 2, 3]);
  var c2 = const C(list: [1, 2, 3]);
}

int calculateSum() => 25;

class B {
  static final b = 5;
}

class C {
  //! final list that will hold constant lists
  final List<int> list;

  const C({
    required this.list
  });
}
