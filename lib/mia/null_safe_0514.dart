int a = 1; //!a=null

class A {
  static int b = 2; //!b=null
}

int? nullable;

class RandomClass {
  int x;
  int y = 7;
  int z;

  RandomClass({
    required this.z,
  }) : x = 5 {
    print('constructor Body');
  }
}

void optionalParam([int beta = 25]) {
  int localVariable;
  localVariable = 5;
  print(10 + localVariable);
}

// void main(List<String> arguments) {
//   print(a);
//
//   print(A.b);
//
//   print(nullable);
//
//   var car= Car();
//   car.accelerate();
//   car.steer();
//
//   //!temperature field is not initialized here
//   var w= Weather();
//   //!but rather here,when accessed was lazily created
//   print(w.temperature +25);
//
//   //!mandatory,order matters
//   positionalMandatory(2, 3);
//   //!optional, order matters
//   positionalOptional(null,3);
//   //!mandatory,order doesn't matter
//   namedMandatory(b: 3,a: 2);
//   //!optional, order doesn't matter
//   namedOptional(b: 3,a: 2);
//
// }

bool isEmptyList(Object object) {
  if (object is! List) {
    return false;
  } else {
    return object.isEmpty;
    //return (object as List).isEmpty
  }
}

class Coffee {
  String? _temperature;

  void heat() {
    _temperature = 'hot';
  }

  void chill() {
    _temperature = 'iced';
  }

  void checkTemp() {
    if (_temperature != null) {
      print('Ready to serve ' + _temperature! + '!');
    }
  }

  String serve() => _temperature! + 'coffee';
}

int tracingFibonacci(int n) {
  final int result;
  if (n < 2) {
    result = n;
  } else {
    result = tracingFibonacci(n - 2) + tracingFibonacci(n - 1);
  }
  print(result);
  return result;
}

//! List<String>? =>List<String> arguments
String makeCommand(String executable,[List<String>? arguments]){
  var result = executable;
  if(arguments !=null){
    result +=''+arguments.join('');
  }
  return result;
}

class HttpResponse{
  final int code;
  final String? error;

  HttpResponse.ok()
  :code = 200,
  error = null;
  HttpResponse.noyFound()
  :code = 404,
  error = 'Not found';
  @override
  String toString(){
    if(code == 200) return 'OK';
    return 'ERROR $code ${error!.toUpperCase()}';
  }
}

class Car{
  // int _speed;
  // int? _speed;
  late int _speed;
  void accelerate(){
    _speed = 50;
  }
  void brake(){
    _speed =0;
  }
  int steer()=>_speed -=15;
// int steer()=> _speed =_speed! - 15;

}

int readThermometer()=>25;
class Weather{
  late final int temperature = readThermometer();
}

void positionalMandatory(int a,int b){}
void positionalOptional([int? a,int? b]){}
void namedMandatory({required int a,required int b}){}
void namedOptional({int? a=1,int? b=1}){}


void main(){
  test(a: 3);
}

void test({required int a}){

}