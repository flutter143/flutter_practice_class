// late int a;

// void main(){
//   print(a);
//   print(A.c);
// var alfa=A(f: 1)..h=2;
// print(alfa.h);
//   A(f: 1);
//   print(A(f: 1).h);
// }
// class A{
//    static late int? b=1;
//    static late int c;
//    late int? d;
//    int e=1;
//    int f;
//    int g;
//    late int h;
//    A({required this.f}):g=1;
// }

// class B {
//   int a;
//   int b;
//
//   B(int a, this.b, int? c, {required int? d, int? e}) : this.a = a;
// }
//
// void main() {
//   B(5, 3, 4, d: 1);
// }

// //required ->必須給
// //a需初始化或？或late或this.
// class C {
//   late int a;
//   int? b;
//   int d = 1;
//
//   C(int a, this.b, int? c, int d, {required int? e, int? f});
// }
//
// //調用
// void main() {
//   C(7, 20, 3, 4, e: 99);
// }
// import 'package:flutter_practice_class/mia/notes.dart';
//
// class C {
//   late int a;
//   int? b;
//   int c = 1;
//
//   C(int a, this.b, int? c);
// }
//
// //調用
// void main() {
//   C(7, 20, 3);
//   print(C(7, 20, 3).a);
//
//
// // 變數
//   int a;
//   late int b;
//
// //差別在於a一直存在，b要調用的時候才會有

// int aa;
// var=>變量，不能換類型，但如果不給職慧根dynamic依樣
// void main() {
// var bb=1;
// bb = 3;
// bb = '123';
// }
// dynamic=>可更換類型，run後才會知道型別
//

// void a() {
// 宣告方式
//   int topLevel = 1;
//   int topLevel1;
//   int? topLevel3;
//   int? topLevel4 = 1;

// 調用方式
// print(topLevel);
// }
//調用
// void main() {
//   C(1, 3);
// }

// class C {
//   int? a;
//
//   // C(int a, int b):this.a=a;
//   C(int a, int b) : this.a = 1;
// // C(int a, int b){a=1;}
// }
//
// class B {
//   int a;
//   int b;
//
//   B(int a, this.b, int? c, {required int? d, int? e}) : this.a = 1;
// }

//調用
//調用constructor會生成一個 B class的物件
//5為參數，需匹配類別
// void main() {
//   A();
// }
// class A {
// //宣告方式
//   int? a;
//   int b = 1;
//   int? c = 1;
//   late int e;
//
// // 在constructor body之前給變數
//   A(int a, int b, {int? c, this.e=1}) {
//     this.e = 1111;
//   }
// }
//
// // 調用方式
// void main() {
//   //constructor 調用
//   A(1, 2, c: 2, e: 3).a;
//   //instance value 調用
//   print(A(5, 4, c: 5, e: 1).e);
// }
// void main() {
// // 宣告方式
//   int topLevel = 1;
//   int topLevel1;
//   int? topLevel3;
//   int? topLevel4 = 1;
//
// // 調用方式
//   print(topLevel);
// }
// class A {
//   // 宣告方式
//   static int? a;
//   static int b = 1;
//   static int? c = 1;
//   static late int e;
// }
//
// // 調用方式
// void main() {
// // A 為class 名字
// // a 為static 變數
// //   A.a;
//   print(A.a);
// }
// class C {
//   late int a;
//   int? b;
//   int d = 1;
//
//   C(int a, this.b, int? c, int d, {required int? e, int? f}):this.a=a;
// }
//
// //調用
// void main() {
//   C(7, 20, 3, 4, e: 99).b;
// }
// class C {
//   late int a;
//   int? b;
//   int d;
//
//   C(int a, this.b, int? c, this.d, {required int? e, int? f});

// void k() {
//這裡的 this. 為調用
// this.a;
// }
// }

//調用
// void main() {
//   C(7, 20, 3, 4, e: 99);
//   print(C(5, 6, 3, 8, e: 6));
// var f=C(1,2,3,4,e: 1);
//  C a=C(1,2,3,4,e: 1);
// a.k();

class B {
  late int a;
  int? b;

  B(int a, this.b, int? c, {required int? d, int? e})
      //this.a = a =>this.instance variable = parameter
      : this.a = a;

  void k() {
//這裡的 this. 為調用
    this.a;
  }
}
//調用
void main() {
  //有存在記憶體內，可重複使用
  B aa = B(5, 3, 4, d: 1);
  // var f= B(5, 3, 4, d: 1);
  //調用B(int a)給予覆值，B(5, 3, 4, d: 1) 的 5 會被覆蓋
  aa.a = 1;
  //調用class裡面的一般function
  aa.k();
  //沒有存在記憶體內，不可重複使用
  B(5, 3, 4, d: 1).k();


  //compile time
  var bb=1;



  //可空可不空 run time才知道對錯
  dynamic a;

  int;
  var z=1;//較彈性，不需要有？
  dynamic;//不需要有？






}


