void main(List<String> arguments){
  double a= 5.0;
  double? b;
  //!intersection of funtions available for both types
  print(a.toString());
  print(b.toString());
  print(a==a);
  print(b==b);
  print(a.hashCode);
  print(b.hashCode);
  //!other funtions are not available for double
  print(a.floorToDouble());
  print(b!.floorToDouble());
}