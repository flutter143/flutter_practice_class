//! top-level variable
int t = 5;
int? nullableTopLevel;
late int nonNullableTopLevel;

class A {
  //! static variable
  static int s = 12;

  static int? nullableStatic;
  static late int nonNullableStatic;

  //! instance variable (field/property)
  double i = 25;
  double? nullableInstance;

  A({
    required this.i,
}){
    nullableInstance =30;
    //? this is the constructor body
  }
}

void randomFunction() {
  //! local variable
  int l = 4;
}

void main(List<String> arguments) {
  nullableTopLevel = 5;
  // print(nullableTopLevel!isEven);

   nonNullableTopLevel = 10;
  //print(nullableTopLevel!.isEven);

  //print(A.s); //調用Static 要打 Class名子.static變數名
  //A.nullableStatic = null;
 // print(A.nullableStatic!.isEven);

  A.nullableStatic = 5;
  // print(A.nullableStatic!.isEven);

  A.nonNullableStatic = 15;
  // print(A.nullableStatic!.isEven);

  var alfa = A(i:10)..nullableInstance = 20;
  print(alfa.nullableInstance);
}
