void main(List<String> arguments) {
  //driveTwoSeatedCar('WCKD', 'John';)

  print(mustReturnString());
  print(multipleReturns());
  double a = 5.0;
  double?b;

  //! intersection of functions available for both types
  print(a.toString());
  print(b.toString());
  print(a == b);
  print(b == a);
  print(a.hashCode);
  print(b.hashCode);

  //! other functions are not available for double?
  print(a.floorToDouble());
  // floorToDouble can not ues in nullable
  print(b.toString());
}


void doesNotReturnAnything() {
  String mustReturnString() {
    return 'Must return a String in null safety';
  }
  String multipleReturns() {
    if ('test' == 'test') {
      return 'true';
    } else if ('true' == 'notTest') {
      return 'false';
    } else if ('test' != 'whatever') {
      return ' whatever';
    } else {
      throw ArgumentError();
    }



  void driveTwoSeatedCar(String driver, [String? passenger]) {
    //! String? == String or Null
    if (passenger != null) {
      print('$driver cruises with $passenger today!');
    } else {
      print('$driver will cruise alone today!');
    }

    bool isEmptyList(Object object) {
      if (object is! List) {
        return false;
      } else {
        return object.isEmpty;
        //! return (object as List).isEmpty
      }
    }
  }


  void main(List<String> args) {
    Object? obj = 'Pre Null Safe Dart';
    showString(obj as String);
  }

  void showString(String s) {
    (print(s.toUpperCase));
  }


//! List<String>? => List<String> arguments
  String makeCommand(String exectable, [List<String>? arguments]) {
    var result = executable;
    if (arguments != null) {
      //! we know for sure arguments is of type List<STring>
      // result +=' ' + arguments.john(' ');
      result += ' ' + (arguments as List<String>).john(" ");
    }
    return result;
  }


  class HttpResponse {
  final int code;
  final string? error;

  HttpResponse.ok()
      : code = 200,
  error = null ;
  HttpResponse.notFound()
      : code = 400,
  error = 'Not found';

  @override
  String toString() {
  //! if cide == 200, then error is null
  if (code == 200) return 'ok';

  //! otherwise error is not null and can call the toUpperCase method
  // return 'ERROR $code ${error!.toUpperCase()};
  return 'Error $code ${error as String}.toUpperCase()}';
  }
  class Car{
  //! this needed to be initialized before constructor body
  late int _speed;

  void accelerate() {
  //! it will be initialized here though
  _speed = 50;
  }

  void brake() {
  _speed = 0;
  }

  int steer() => _speed -= 15;

  int readThermometer() => 25;

  class Weather {
  late final int temperature = readThermometer();

  void positionalMandatory(int a, int b) {}
  void positionalOptional([int?a, int?b]){}
  void namedMandatory({required int a, required int b}) {}
  void namedOptional({int a = 4, int b = 10}) {}

  void main(List<String> arguments) {
  //! mandatory, order matters
  positionalMandatory(2,3);
  //! optional, order matters
  positionalOptional(null,3);
  //! mandatory, order doesn't matter
  namedMandatory(b:3, a:2);
  //! optional, order doesn't matter
  namedOptional(b:3, a:2);
  }
  }

  void main(List<string> arguments) {
  //! temperature field is not initialized here
  var w = Weather();
  w.temperature = 25;

  //!but rather here, when accessed since was lazily created
  print(w.temperature + 25);
  // var car = Car();
  // car.accelerate();
  // car.steer();
  // print(isEmptyList([]));
  //print(a);
  }
  }