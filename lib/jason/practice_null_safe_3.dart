void main(List<String> arguments) {
  double a = 5.0;
  double? b;
  //!intersection of functions avaiable for both types
  print(a.toString());
  print(b.toString());
  print(a == b);
  print(b == b);
  print(a.hashCode);
  print(b.hashCode);
  //!other functions are not available for double
  print(a.floorToDouble());
  print(b!.floorToDouble());
}
