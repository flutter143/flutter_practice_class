class Car {

 late int _speed ;

  void accelerate() {
    _speed = 50;
  }

  void brake() {
    _speed = 0;
  }

  int steer() => _speed -= 15;
}

void main(List<String> arguments) {
  var car = Car();
  //! temperature field is not initialized here
  var w = Weather();

  //! but rather here, when accessed since was lazily created
  print(w.temperature + 25);

  car.accelerate();
}

int readThermometer() => 25;

class Weather {
  late int temperature = readThermometer();
}



