void main(List<String> arguments) {
  print(mustReturnString());
  print(multipleReturns());
}

void doesNotReturnAnything() {}

String mustReturnString() {
  return 'Must return a String in null safety';
}

String multipleReturns() {
  if ('test' == 'test') {
    return 'ture';
  } else if ('test' == 'notTest') {
    return 'fales';
  } else if ('test' != 'whatever') {
    return 'whatever';
  } else {
    throw ArgumentError(); //! return Never
  }
}
