//!top_level variable
int t=5;
int? nullableTopLevel;
late int nonNullableTopLevel;

class A {
  //!static variable
  static int s = 12;
  static int? nullableStatic = 5;
  static late int nonNullableStatic;

  //!instance variable (field/property
double i = 25 ;
A({
    required this.i,
});
}

void randomFunction() {
  //!local variable
  int l = 4;
}

void main(List<String> argument) {
  t=8;
  nullableTopLevel = null;
  //print(nullableTopLevel!.isEven);

  //nonNullableTopLevel=10;
  print(nonNullableTopLevel.isEven);

  print(A.s);
  //A.nullableStatic = null;
  //print(A.nullableStatic!.isEven);

  A.nullableStatic=5;
  //print(A.nullableStatic!.isEven);
  A.nonNullableStatic=15;
  //print(A.nonNullableStatic.isEven);

  print(A(i:20).i);
}