void main(List<String> arguments) {
  driveTwoSeatedCar('WCKD', null);
}

void driveTwoSeatedCar(String driver, [String? passenger]) {
  if (passenger != null) {
    print('$driver cruises with $passenger today!');
  } else {
    print('$driver will cruise alone today!');
  }
}
