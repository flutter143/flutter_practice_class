void main(List<String> arguments) {
  const int a = 5; // int a = const int(value:5)

  //! a is const, so it cannot be reassigned
  // a =7;
  // a =8;
  // a =9;

  //! list is const, then it means [1,2,3] is const
  const list = [1,2,3];

  // ignore: lines_longer_than_80_chars
  //! if the value is const [1,2,3], it does not mean variable list2 is also const!
  var list2 = const [1,2,3];

  final b = 6;

  //! b can't be reassigned since it's final
  //b =7;

  //! final list3 does not mean [1,2,3] list is const!
  final list3 = [1,2,3];

  //! sinec [1,2,3] is not const, you can mutate its content
  list3.add(4);
  list3.add(5);
  list3.add(6);

  //! yet list3 is still a final variable, so it can't be reassigned
  // list3 = [4,5,6];

  //! now [1,2,3] is a const list
  //! final wars will have a value known at runtime
  final list4 = const [1,2,3]; // const list4 = [1,2,3]

  // list4.add(5);
  // list4.add(6);
  // list4.add(7);

  //! const vars will have a value known at compiletime
  //! [1,2,3] is const
  const list5 = [1,2,3];


  // print(list4.hashCode);
  // print(list5.hashCode);

  final alfa = A();
  alfa.a = 10;
  //print(alfa.a);
  final sum = calculateSum();

  var c1 = const C(list: [1,2,3]);
  var c2 = const C(list: [1,2,3]);

  print(c1.hashCode);
  print(c2.hashCode);
}

int calculateSum() => 25;

class A {
 int a = 5;
}

class B {
  static const b=5;
}

class C {
  final List<int> list;
  const C({
    required this.list,
});
}