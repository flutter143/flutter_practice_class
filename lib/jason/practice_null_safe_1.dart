void main(List<String> arguments) {
  driveTwoSeatedCar('WCKD', 'John');
}

void driveTwoSeatedCar(String driver, [String? passenger]) {
  //!String?==String or Null
  if (passenger != null) {
    print('$driver cruises with $passenger today!');
  } else {
    print('$driver will cruise alone today!');
  }
}